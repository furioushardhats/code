#ifndef HD44780_H_
#define HD44780_H_

#include "stm32l4xx_hal.h"

typedef struct
{
  GPIO_TypeDef * LCD_GPIO;
  uint16_t D4_PIN;
  uint16_t D5_PIN;
  uint16_t D6_PIN;
  uint16_t D7_PIN;
  uint16_t EN_PIN;
  uint16_t RS_PIN;
  uint16_t LCD_EN_DELAY;
}HD44780_CfgType;

// Function Prototypes

void LCD_Delay_us(uint16_t);                        //Microsecond delay for timing
void LCD_Init();                                    //Initialize the LCD in 4-bit mode
void LCD_Clear();                                   //Clear the LCD screen
void LCD_CMD(unsigned char);                        //Send command to LCD
void LCD_DATA(unsigned char);                       //Send 4-bit data to LCD
void LCD_Set_Cursor(unsigned char, unsigned char);  //Set LCD cursor position
void LCD_Write_Char(char);                          //Write character at cursor
void LCD_Write_String(char*);                       //Write string to LCD
void LCD_SL();                                      //Shift display to the left
void LCD_SR();                                      //Shift display to the right

#endif /* HD44780_H_ */

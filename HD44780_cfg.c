#include "HD44780.h"

const HD44780_CfgType HD44780_CfgParam =
{
  GPIOB,        //MCU Port used
  GPIO_PIN_4,   //D4 Pin
  GPIO_PIN_5,   //D5 Pin
  GPIO_PIN_6,   //D6 Pin
  GPIO_PIN_7,   //D7 Pin
  GPIO_PIN_1,   //EN Pin
  GPIO_PIN_0,   //RS Pin
  50,           //EN Delay
};
